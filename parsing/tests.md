title: Mais enfin !?

Tout ceci est, tout de même,

# De plus en plus curieux...

J’en ai bien peur et il me faut l’admettre, il semble que je sois complètement
et irrémédiablement [perdu][][^fn].

[perdu]: https://perdu.com

## Balises de détresse

Tirons <sup>bien **haut**</sup> une <mark>[balise][]</mark>.

[balise]: https://media.senscritique.com/media/000007591339/960/Coke_en_stock_Les_Aventures_de_Tintin_tome_19.jpg

<p>Y a-t-il du **Markdown** dans l’avion ?</p>
<p markdown>Y a-t-il du **Markdown** dans l’avion ?</p>
<div markdown>Y a-t-il du **Markdown** dans le `<div>` ?</div>

\#ABCÇDEÉFGHIJKLMNOPQRSTUVWXYZ’.  
\#abcçdeéfghijklmnopqrstuvwxyz’.  
Ce paragraphe a des attributs personnalisés.  
{: #custom-par-id .prev style="font-family: Georgia;" }

[^escape]: Tout compte fait, oublions cette idée, elle est naâaze.

[^nbsp]:
    Noter l’utilisation d’une espace échappée `\ ` qui, avec l’extension
    idoine, devrait produire une espace insécable (quoique non fine)[^escape].

[^fn]: Voir conditions en magasin[^ordre][^fnfn].


[^ordre]: Ainsi donc l’ordre des notes n’est pas automatique…
[^fnfn]: Des notes récursives[^fnfn] ? Aucun problème !

## Smarties

9. Un smartie's simple, c'est toujours meilleur.
  - Ce smartie's était trop 'intelligent' pour son propre bien ;
    nous l’avons 'désmarté'...
9. Un smartie's "double".
9. Un smartie's <<\ à la française\ >>[^nbsp].
9. Un smartie's qui attend...
9. Un smartie's qui décolle de Roissy--Charles-de-Gaulle.
9. C’était ---\ je crois\ --- par une nuit smarteuse[^nbsp].

## Listes

**Compacte** = tous les items collés

**Aérée** = une ligne vide entre chaque item.

Une liste à tirets
collée au paragraphe
qui le précède:
- Pif.
- Paf.
- Pouf (c’est un bon début).

Une liste numérotée
collée au paragraphe
qui le précède:
9. Pif.
9. Paf.
9. Pouf.

Une liste à tirets avec des continuations :

- Pif.
  Pif bis.
  Pif ter.
 Pas pif.
  Pas pif-bis.
- Paf.
- Pouf.

Une liste numérotée avec des continuations :

9. Pif.
   Pif bis.
   Pif ter.
  Pif sauf si yolo.
 Jamais pif.
   Pas pif-bis.
9. Paf.
9. Pouf.

Une liste à tirets compacte indentée de 0+2 :

- Pif.
  Pif bis.
   Pif ter.
    Pif quad.
     Pif quinquiès.
      Pif sexa.
- Paf.
- Pouf.

La même indentée de 0+10 :

- Pif.
          Pif bis.
- Paf.
- Pouf.

La même indentée de 1+2 (mode yolo) :

 - Pif.
   Pif bis.
 - Paf.
 - Pouf.

La même indentée de 2+2 (mode yolo) :

  - Pif.
    Pif bis.
  - Paf.
  - Pouf.

La même indentée de 3+2 (mode yolo) :

   - Pif.
     Pif bis.
   - Paf.
   - Pouf.

La même indentée de 4+2 (`<pre>`) :

    - Pif.
      Pif bis.
    - Paf.
    - Pouf.

Une liste numérotée compacte indentée de 0+2 (cassée en mode yolo) :

9. Pif.
  Pif bis.
9. Paf.
9. Pouf.

La même indentée de 1+2 (cassée) :

 9. Pif.
   Pif bis.
 9. Paf.
 9. Pouf.

La même indentée de 0+3 :

9. Pif.
   Pif bis.
9. Paf.
9. Pouf.

La même indentée de 0+10 :

9. Pif.
          Pif bis.
9. Paf.
9. Pouf.

La même indentée de 1+3 (mode yolo) :

 9. Pif.
    Pif bis.
 9. Paf.
 9. Pouf.

La même indentée de 2+3 (mode yolo) :

  9. Pif.
     Pif bis.
  9. Paf.
  9. Pouf.

La même indentée de 3+3 (mode yolo) :

   9. Pif.
      Pif bis.
   9. Paf.
   9. Pouf.

La même indentée de 4+3 (`<pre>`) :

    9. Pif.
       Pif bis.
    9. Paf.
    9. Pouf.

Une liste à tirets semi-aérée indentée de 0+2 :

- Pif.
  Pif bis.

- Paf.
- Pouf.

Une liste à tirets aérée indentée de 0+2 :

- Pif.
  Pif bis.

- Paf.

- Pouf.

Une liste numérotée semi-aérée indentée de 0+3 :

9. Pif.
   Pif bis.

9. Paf.
9. Pouf.

Une liste numérotée aérée indentée de 0+3 :

9. Pif.
   Pif bis.

9. Paf.

9. Pouf.

Une liste à tirets compacte mais avec des paragraphes :

- Pif.

  Pif bis.
- Paf.
- Pouf.

Une liste numérotée compacte mais avec des paragraphes :

9. Pif.

   Pif bis.
9. Paf.
9. Pouf.

Une liste à tirets (0+2) compacte avec des sous-listes :

- Pif.
  - Sous-pif pif.
  - Sous-pif paf.
  - Sous-pif pouf.
  Pif bis.
- Paf.
  - Sous-paf pif.
  - Sous-paf paf.
  - Sous-paf pouf.
  Paf bis.
- Pouf.
  - Sous-pouf pif.
  - Sous-pouf paf.
  - Sous-pouf pouf.
  Pouf bis.

La même aérée au premier niveau (chaque item est compact) :

- Pif.
  - Sous-pif pif.
  - Sous-pif paf.
  - Sous-pif pouf.
  Pif bis.

- Paf.
  - Sous-paf pif.
  - Sous-paf paf.
  - Sous-paf pouf.
  Paf bis.

- Pouf.
  - Sous-pouf pif.
  - Sous-pouf paf.
  - Sous-pouf pouf.
  Pouf bis.

La même aérée au premier niveau (les sous-listes sont compactes mais entourées
de lignes vides) :

- Pif.

  - Sous-pif pif.
  - Sous-pif paf.
  - Sous-pif pouf.

  Pif bis.

- Paf.

  - Sous-paf pif.
  - Sous-paf paf.
  - Sous-paf pouf.

  Paf bis.

- Pouf.

  - Sous-pouf pif.
  - Sous-pouf paf.
  - Sous-pouf pouf.

  Pouf bis.

Une liste à tirets semi-aérée avec diverses indentations à l’intérieur :

- Pif.

  Paragraphe indenté de 2.

   Paragraphe indenté de 3.

    Paragraphe indenté de 4.

     Paragraphe indenté de 5.

      Paragraphe indenté de 6 (code).

       Paragraphe indenté de 7.

        Paragraphe indenté de 8.

- Paf.
- Pouf.

Une liste numérotée avec diverses indentations à l’intérieur :

9. Pif.

   Paragraphe indenté de 3.

    Paragraphe indenté de 4.

     Paragraphe indenté de 5.

      Paragraphe indenté de 6 (code sauf si yolo).

       Paragraphe indenté de 7 (code).

        Paragraphe indenté de 8.

9. Paf.
9. Pouf.

Une liste dont le seul item contient un paragraphe, du code, un paragraphe (il
ne doit pas y avoir de ligne vide au milieu du bloc de code) :

- Pif.

      - Pas sous-pif pif.
      - Pas sous-pif paf.
      - Pas sous-pif pouf.
  Pif bis.

Pareil mais avec un bloc de code fencé (seulement avec l’extension idoine) :

- Pif.

  ```
  - Pas sous-pif pif.
  - Pas sous-pif paf.
  - Pas sous-pif pouf.
  ```

  Pif bis.

Une liste dont le seul item est soit un bloc de code (sans yolo), soit deux
paragraphes suivis d’une sous-liste :

-     Pif.

      Pif bis.
      + Sous-pif plus.

Plusieurs types de listes consécutives :

- Tiret.
- Tiret.
+ Plus.
+ Plus.
1. Nombre 1.
2. Nombre 2.

### Poèmes

. Les points sont-ils compris ? On le verra tantôt
. Suffit de compiler pour voir le résultat
. Si les points sont compris, ça devrait être beau
. Et dans le cas contraire, ça fera du fatras.
.
. Les points sont-ils compris ?
. Je le saurai bien vite
. En compilant ce site,
. Mildiou de sapristi !

### Listes à tirets doubles

-- Une liste à tirets doubles.
-- Le deuxième item.
--Le troisième item (en fait, il n’y a pas besoin d’espace !)
--  Item avec 2 espaces.
--   Item avec 3 espaces.
--    Item avec 4 espaces (code sauf si yolo).

### Listes de définitions

; premier terme

: première définition
: deuxième définition du même terme
  (eh oui, c’est possible en HTML)
; deuxième terme
; troisième terme

  (eh oui, ça aussi c’est permis)
: définition des termes 2 et 3

### En mode yolo

  -  Il était
     une fois
     (une seule)
       - une liste qui volait
                     dans l’espace
     
      -   en s’alignant
             comme bon
                     lui semblait
     -                            elle était libre
                                  - comme l’air
                                 - et n’écoutait
    - personne
