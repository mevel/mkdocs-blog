"""
    Documentation about writing extensions for Python-Markdown:
        https://python-markdown.github.io/extensions/api/
    Documentation about the xml library:
        https://docs.python.org/3/library/xml.etree.elementtree.html
    Implementation of builtin processors, for inspiration:
        https://github.com/Python-Markdown/markdown/blob/master/markdown/blockprocessors.py
        and the other files
"""

from markdown.preprocessors import Preprocessor
from markdown.blockprocessors import BlockProcessor
from markdown.treeprocessors import Treeprocessor as TreeProcessor
from markdown.extensions import Extension
import re
import xml.etree.ElementTree as xml

class MyPreproc(Preprocessor):
    def run(self, lines):
        return filter((lambda line: not re.search("smart", line)), lines)

class MyBlockProc(BlockProcessor):
    def test(self, parent, block):
        return True
    def run(self, parent, blocks):
        #print(self.tab_length)
        return False

class MyTreeProc(TreeProcessor):
    def run(self, root):
        el = xml.Element('h2')
        el.set('style', 'color: red !important;')
        it = xml.SubElement(el, 'em');
        it.text = 'Titre obtenu par matérialisation spontanée'
        it.tail = ' (effet non garanti)'
        root.append(el)
        return None

class MyExtension(Extension):
    def __init__(self, **kwargs):
        self.config = {
            'option1' : [ 'value1', 'description 1' ],
            'option2' : [ 'value2', 'description 2' ],
        }
        super(MyExtension, self).__init__(**kwargs)
    def extendMarkdown(self, md):
        md.preprocessors.register(MyPreproc(md.parser), 'mypreproc', 42)
        md.parser.blockprocessors.register(MyBlockProc(md.parser), 'myblockproc', 42)
        md.treeprocessors.register(MyTreeProc(md), 'mytreeproc', 42)
