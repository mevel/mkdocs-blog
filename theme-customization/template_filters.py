# A “filter” function ‘f’ is called from Jinja like this: {{ x | f }}.
# It can take several arguments but the syntax is awkward: {{ x | f(y, z) }}.
#
# A “global” function ‘f’ is called from Jinja like this: {{ f(x, y, z) }}.
# It can take several arguments.
#
# https://stackoverflow.com/questions/30337155/app-template-filter-with-multiple-arguments

custom_filters = [ ]
custom_globals = [ ]

def make_filter(f):
    custom_filters.append(f)
    return f

def make_global(f):
    custom_globals.append(f)
    return f

def on_env(env, config, files, **kwargs):
    global custom_filters, custom_globals
    for f in custom_filters:
        env.filters[f.__name__] = f
    for f in custom_globals:
        env.globals[f.__name__] = f
    return env

from dataclasses import dataclass
from datetime import date
import locale

locale.setlocale(locale.LC_TIME, 'fr_FR.UTF-8')

# Dates can be a precise day ('YYYY-MM-DD'), a month ('YYYY-MM') or
# just a year ('YYYY'). YYYY is exactly 4 digits, MM and DD are exactly
# 2 digits. Hence, dates can be compared by comparing them as strings.
@dataclass
class LooseDate:
    input : str
    date : date
    has_day : bool = True
    has_month : bool = True
    has_year : bool = True

    @classmethod
    def parse(cls, input):
        # Apparently the YAML parser for meta-data already transforms things
        # of the form YYYY into integers and those of the form YYYY-MM-DD into
        # date objects (which is annoying, because when it is recognized as
        # such but produces an invalid date, the whole meta-data parsing fails…
        # ah the joys of dynamic typing and loose semantics).
        # So we start by converting it back to a string, in all cases.
        input = str(input)
        # `date.fromisoformat(…)` parses a str against the format 'YYYY-MM-DD'
        # with exactly that many digits and dashes (as of Python 3.10.0, it
        # only accepts years between 0001 and 9999), so we use it to enforce
        # our format described above. It also performs range checking.
        has_day = True
        has_month = True
        # 'YYYY-MM-DD'
        try: D = date.fromisoformat(input)
        except ValueError:
            has_day = False
            # 'YYYY-MM'
            try: D = date.fromisoformat(input + '-01')
            except ValueError:
                has_month = False
                # 'YYYY'
                try: D = date.fromisoformat(input + '-01-01')
                except ValueError:
                    raise ValueError('html_date: ISO 8601 date expected but %r found' % input)
        return cls(input=input, date=D, has_day=has_day, has_month=has_month)

    def format(self):
        # NOTE: Modifiers `4` (pad to 4 digits) and `-` (remove padding zeros)
        # are Glibc extensions. They are not available on all systems.
        #
        # Compute the human-readable format.
        ordinal = '<sup>er</sup>' if self.date.day == 1 else ''
        d = '%-d' + ordinal if self.has_day else None
        m = '%B' if self.has_month else None
        y = '%-Y' if self.has_year else None
        human_fmt = '&nbsp;'.join(filter(None, (d, m, y)))
        # Compute the machine-friendly format, as expected by the <time> tag.
        ##if self.has_day:
        ##    machine_fmt = '%4Y-%m-%d'
        ##elif self.has_month:
        ##    machine_fmt = '%4Y-%m'
        ##else
        ##    machine_fmt = '%4Y'
        # In fact we can reuse the input string directly, because our input
        # format is normalized and matches the expected format exactly.
        machine_fmt = self.input
        return self.date.strftime('<time datetime="{}">{}</time>'.format(machine_fmt, human_fmt))

@make_filter
def html_date(loose_date):
    D = LooseDate.parse(loose_date)
    assert D.has_year and (D.has_month or not D.has_day)
    if D.has_day: # date starts with the day number
        det = 'le '
    else: # day starts with the month name or year number
        det = 'en '
    return det + D.format()

@make_global
def html_date_range(loose_date_start, loose_date_end):
    if loose_date_start == loose_date_end:
        return html_date(loose_date_start)
    # Parse both dates.
    D1 = LooseDate.parse(loose_date_start)
    D2 = LooseDate.parse(loose_date_end)
    # Abbreviate the start date if year/month are the same as in the end date.
    if D1.date.year == D2.date.year and D1.has_month:
        D1.has_year = False
        if D1.date.month == D2.date.month and D1.has_day:
            D1.has_month = False
    # Find what determinant to prefix the start date with.
    assert (D1.has_day or D1.has_month or D1.has_year) \
        and not (D1.has_day and not D1.has_month and D1.has_year)
    if D1.has_day: # date starts with the day number
        det1 = 'du '
    elif D1.has_month: # date starts with the month name
        det1 = 'd’' if D1.date.month in [4, 8, 10] else 'de '
    else: # date starts with the year number
        det1 = 'de '
    # Find what determinant to prefix the end date with.
    assert D2.has_year and (D2.has_month or not D2.has_day)
    if D2.has_day: # date starts with the day number
        det2 = 'au '
    else: # day starts with the month name or year number
        det2 = 'à '
    # Put everything together.
    return '{}{} {}{}'.format(det1, D1.format(), det2, D2.format())
