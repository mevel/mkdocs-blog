# Sort items in the navigation following a custom order.
#
# For each nav section (including the root), children are ordered like so:
#   0. the index page
#   1. pages without a meta.date
#   2. pages with a meta.date, sorted by descending meta.date
#   3. sections
#   4. external links
# Within each bucket, items are ordered by title (instead of file name).

def date_sort_key(date):
    # Dates can be a precise day ('YYYY-MM-DD'), a month ('YYYY-MM') or
    # just a year ('YYYY'); YYYY is exactly 4 digits, MM and DD are 2 digits.
    # Normalize dates for easier comparison (this wouldn’t be needed if we
    # wanted to sort dates in ascending order; in that case the direct string
    # comparison would be enough):
    if len(date) < 10:
        date += '-00'
        if len(date) < 10:
            date += '-00'
    assert len(date) == 10
    # Reverse the date order (most recent date first):
    return date.translate(str.maketrans('0123456789', '9876543210'))

def item_sort_key(item):
    # Recursively compute the sort key of parent items:
    if item.parent:
        pkey = item_sort_key(item.parent)
    else:
        pkey = []
    # Compute the sort key of this item:
    if item.is_page:
        if item.is_index:
            key = (0, item.title)
        elif 'date' in item.meta:
            # We must convert date metadata to strings, because the YAML parser
            # may have converted them into objects of heterogeneous classes.
            key = (2, date_sort_key(str(item.meta['date'])), item.title)
        else:
            key = (1, item.title)
    elif item.is_section:
        key = (3, item.title)
    elif item.is_link:
        key = (4, item.title)
    else:
        assert False
    pkey.append(key)
    return pkey

# There is an event `on_nav` triggered after the nav object is created but, at
# that point, Markdown files have not been read yet, so titles and metadata are
# missing. Files are then loaded one by one. It is only after every file has
# been read that the nav object has all data, and that MkDocs proceeds to
# generating the HTML pages using the theme templates. This is when we reorder
# pages. The event `on_page_context` is triggered just before generating
# each page. We hook on this event and perform reordering once, the first time
# it is triggered.

has_sorted_nav = False

def on_page_context(context, page, config, nav, **kwargs):
    global has_sorted_nav
    if has_sorted_nav: return
    has_sorted_nav = True
    # Sort the flat list of all navigable pages:
    nav.pages.sort(key=item_sort_key)
    # Sort the navigation tree:
    item_lists = [ nav.items ]
    while item_lists:
        item_list = item_lists.pop()
        item_list.sort(key=item_sort_key)
        for item in item_list:
            if item.children:
                item_lists.append(item.children)
    # Fix the prev/next links:
    if nav.pages:
        pages = iter(nav.pages)
        prev = next(pages)
        prev.previous_page = None
        for cur in pages:
            prev.next_page = cur
            cur.previous_page = prev
            prev = cur
        prev.next_page = None
